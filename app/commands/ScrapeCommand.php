<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScrapeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eggs:scrape';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Insert all words into the table';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$handle = fopen("/usr/share/dict/words", "r");
	    while (($line = fgets($handle)) !== false) {
	    	$line = substr($line,0,-1);
	    	if ("'s"==substr($line,-2)) {
	    		continue;
	    	}
			$word = new Word;
			$word->word = $line;
			$word->save();
	        echo $line."\n";
	    }
		fclose($handle);
		exit;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
