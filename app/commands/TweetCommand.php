<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TweetCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eggs:tweet';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send a tweet.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$word = Word::findRandomUntweeted();
		if ($word) {
			echo($word->word."\n");
			/*
			$this->tweetWord($word->word);
			$word->tweeted=1;
			$word->save();
			*/ 
		}
		exit;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

	protected function tweetword($word) 
	{
		$tweetText = "eggs {$word}";
		Twitter::postTweet(
	        array(
	            'status' => $tweetText,
	            'format' => 'json'
	        )
		);
	}

}
